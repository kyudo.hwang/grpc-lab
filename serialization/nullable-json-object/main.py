import json
import gzip
import person_pb2 as person_pb2  # proto.person_pb2 should be generated from the .proto file
from google.protobuf.json_format import MessageToJson

person_without_email = {
    "id": 123,
    "name": "John Doe",
    "email": None  # JSON uses `null`
}

json_data_without_email = json.dumps(person_without_email).encode('utf-8')

person_proto_without_email = person_pb2.Person()
person_proto_without_email.id = person_without_email["id"]
person_proto_without_email.name = person_without_email["name"]

proto_data_without_email = person_proto_without_email.SerializeToString()

compressed_json_data_without_email = gzip.compress(json_data_without_email)
compressed_proto_data_without_email = gzip.compress(proto_data_without_email)

json_size_without_email = len(compressed_json_data_without_email)
proto_size_without_email = len(compressed_proto_data_without_email)

print(f"JSON without email: {person_without_email}")
print(f"JSON without email: {json_size_without_email} bytes")
print(f"Proto without email: {proto_size_without_email} bytes")
