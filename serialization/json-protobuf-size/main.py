import json
from person_pb2 import Person  # Protobuf 파일에서 생성된 모듈
from gzip import compress

# 데이터 정의
data = {
    "id": 1234,
    "name": "John Doe",
    "email": "jdoe@example.com",
}

# JSON 직렬화
json_data = json.dumps(data).encode('utf-8')
compressed_json_data = compress(json_data)
json_size = len(compressed_json_data)

# Protobuf 직렬화
person = Person()
person.id = data['id']
person.name = data['name']
person.email = data['email']
protobuf_data = person.SerializeToString()
compressed_protobuf_data = compress(protobuf_data)
protobuf_size = len(compressed_protobuf_data)

# 크기 비교 출력
print(f"JSON: {json_data}")
print(f"JSON size: {json_size} bytes")
print(f"Protobuf size: {protobuf_size} bytes")