from example_pb2 import TestMessage

def to_hex_string(byte_data):
    return ' '.join(f'{byte:02x}' for byte in byte_data)

# int32로 값 설정
msg_int32 = TestMessage(field_int32=2147483647)
binary_int32 = msg_int32.SerializeToString()

# int32로 다른 값 설정
msg_int32_2 = TestMessage(field_int32=2147483647)
binary_int32_2 = msg_int32_2.SerializeToString()

# uint32로 값 설정
msg_uint32 = TestMessage(field_uint32=4294967295)
binary_uint32 = msg_uint32.SerializeToString()

# bool로 값 설정
msg_bool = TestMessage(field_bool=False)
binary_bool = msg_bool.SerializeToString()

# 16진수로 출력
print("int32:", to_hex_string(binary_int32))
print("int32_2:", to_hex_string(binary_int32_2))
print("uint32:", to_hex_string(binary_uint32))
print("bool:", to_hex_string(binary_bool))