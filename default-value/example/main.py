import example_pb2

# TestMessage 객체 생성
message = example_pb2.TestMessage()

# 기본값 출력
print('-- Default Value --')
# print(f"id: '{message.id if message.HasField("id") else "None"}'") # AttributeError: 'TestMessage' object has no attribute 'id'
print(f"id: {message.id}")
print(f"name: '{message.name}'")
print(f"is_active: {message.is_active}")
print(f"score: {message.score}")


# Wrapper 객체 생성
print('\n-- Wrapper --')
wrapped_message = example_pb2.TestMessageWrapper()
print(f"id: {wrapped_message.id if wrapped_message.HasField("id") else "None"}")
print(f"name: '{wrapped_message.name.value if wrapped_message.HasField("name") else "None"}'")
print(f"is_active: {wrapped_message.is_active.value if wrapped_message.HasField("is_active") else "None"}")
print(f"score: {wrapped_message.score.value if wrapped_message.HasField("score") else "None"}")
