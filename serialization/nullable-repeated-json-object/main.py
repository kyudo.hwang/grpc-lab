import json
import gzip
import person_pb2


persons = []
for i in range(10):
    person = person_pb2.Person()
    person.id = i
    person.name = f"John Doe {i}"
        
    persons.append(person)

# Create PersonList message
person_list = person_pb2.PersonList()
person_list.persons.extend(persons)

# Serialize PersonList to string
proto_data = person_list.SerializeToString()

# Convert PersonList to JSON format for comparison
person_list_json = {
    "persons": [
        {
        "id": person.id,
            "name": person.name,
            "email": None
        } for person in persons
    ]
}

# JSON serialization
json_data = json.dumps(person_list_json).encode('utf-8')

# Compress the data using gzip
compressed_proto_data = gzip.compress(proto_data)
compressed_json_data = gzip.compress(json_data)

# Compare the sizes
proto_size = len(compressed_proto_data)
json_size = len(compressed_json_data)

print(f"JSON: {json_size} bytes")
print(f"Proto: {proto_size} bytes")